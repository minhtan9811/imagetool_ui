import axios from 'axios';

const route_api = 'http://localhost:5000'
//const route_api = 'http://127.0.0.1:8000'
// const route_api = 'https://like2like.org'
const instance = axios.create({
	baseURL: `${route_api}/`,
	timeout: 1000,
	headers: { authorization: sessionStorage.getItem('like2like_token') }
});

const api = {
	route: `${route_api}/api`,
	instance,
	notify: (message, type) => {
		window.$.notify({
			// options
			message
		}, {
			// settings
			type,
			delay: 3000,
		});
	},
	login: (params) => instance.post('/auth/login', params),
	upload: (params) => instance.post('/upload/post', params),
	logout: () => instance.post('/auth/logout'),
	// me: () => instance.get('/user/me'),
	// updateProfile: (params) => instance.post('/user/profile', params),
	//
	// createRequest: (params) => instance.post('/request/post_fb', params),
	// loadRequests: () => instance.get('/request/post_fb'),
	// loadRequestsLike: () => instance.get('/request/post_fb_like'),
	// detailRequest: (params) => instance.post('/request/post_fb_detail', params),
	// likeRequest: (params) => instance.post('/request/like_post', params),
	// loadPreviewPost: () => instance.get('/request/like_post_preview'),
	// loadLikedPost: () => instance.get('/request/like_post_history'),
	//
	// getWallet: () => instance.get('/wallet/info'),
	// getTrans: () => instance.get('/wallet/transactions'),
	// request2FA: (params) => instance.post('/wallet/require-2fa', params),
	// withdraw: (params) => instance.post('/wallet/withdraw', params),
}

export default api;