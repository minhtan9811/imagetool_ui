import React from 'react'
import logo from '../logo.png'
import avatar from '../../src/avatar.png';

import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { useEffect, useState } from 'react';

function SideBar() {

	const user = useSelector(state => state.User);
	const [name] = useState(sessionStorage.getItem('name'));

	return (
		<div className="sidenav" id="sidenav-main">
			<div className="sidenav-header d-flex align-items-center">
				<Link className="navbar-brand" to="/app/add-frame-page"> <img src={logo}
					className="navbar-brand-img" alt="..."
					width="100" />
				</Link>
				<div className="ml-auto">
					<div className="sidenav-toggler sidenav-toggler-dark d-md-none" data-action="sidenav-unpin"
						data-target="#sidenav-main">
						<div className="sidenav-toggler-inner">
							<i className="sidenav-toggler-line bg-white"></i> <i
								className="sidenav-toggler-line bg-white"></i> <i
									className="sidenav-toggler-line bg-white"></i>
						</div>
					</div>
				</div>
			</div>
			<div
				className="sidenav-user d-flex flex-column align-items-center justify-content-between text-center">
				<div>
					<a href="#" className="avatar rounded-circle avatar-xl"> <img alt="Image placeholder"
						src={avatar}
						className="" /> </a>
					<div className="mt-4">
						<h5 className="mb-0 text-white">{name}</h5>
					</div>
				</div>
			</div>
		</div>
	);
}

export default SideBar