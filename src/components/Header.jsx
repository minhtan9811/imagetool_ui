import React from 'react'
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from 'react';

import { updateProfile } from "../redux/actions/User";
import API from './../utils/api'

function Header() {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const user = useSelector(state => state.User);
	const [name] = useState(sessionStorage.getItem('name'));
	const logout = () => {
		console.log('logout');
		API.logout()
			.then((respone) => {
				console.log(respone);
				if (respone.status == 200) {
					sessionStorage.clear();
					API.instance.defaults.headers.authorization = '';
					navigate('/login', { replace: true });
				}

			});
	};



	return (
		<nav className="navbar navbar-main navbar-expand-lg navbar-dark bg-primary navbar-border"
			id="navbar-main">
			<div className="container-fluid">
				<button className="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbar-main-collapse"
					aria-controls="navbar-main-collapse" aria-expanded="false"
					aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="navbar-user d-lg-none ml-auto">
					<ul className="navbar-nav flex-row align-items-center">
						<li className="nav-item dropdown dropdown-animate">
							<a className="nav-link pr-lg-0" href="#" role="button" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
								<i className="fas fa-user"></i>
							</a>
							<div
								className="dropdown-menu dropdown-menu-sm dropdown-menu-right dropdown-menu-arrow">
								<h6 className="dropdown-header px-0">{name}</h6>
								<Link to="/app/add-frame-page" className="dropdown-item">
									<i className="fas fa-tasks"></i>
									<span>Add Frame</span>
								</Link>
								<div className="dropdown-divider"></div>
								<a className="dropdown-item" onClick={logout}> <i
									className="fas fa-sign-out-alt"></i> <span>Logout</span> </a>
							</div>
						</li>
					</ul>
				</div>
				<div className="collapse navbar-collapse navbar-collapse-fade" id="navbar-main-collapse">
					<ul className="navbar-nav align-items-lg-center">
						<li className="nav-item ">
							<Link className="nav-link pl-lg-0" to="/app/add-frame-page"> Home </Link>
						</li>
					</ul>
					<ul className="navbar-nav ml-lg-auto align-items-center d-none d-lg-flex">
						<li className="nav-item">
							<a href="#" className="nav-link nav-link-icon sidenav-toggler"
								data-action="sidenav-pin"
								data-target="#sidenav-main"><i className="fas fa-bars"></i></a>
						</li>
						<li className="nav-item dropdown dropdown-animate">
							<a className="nav-link pr-lg-0" href="#" role="button" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
								<i className="fas fa-user"></i>
							</a>
							<div
								className="dropdown-menu dropdown-menu-sm dropdown-menu-right dropdown-menu-arrow">
								<h6 className="dropdown-header px-0">{name}</h6>
								<Link to="/app/add-frame-page" className="dropdown-item">
									<i className="fas fa-tasks"></i>
									<span>Add Frame</span>
								</Link>
								<div className="dropdown-divider"></div>
								<a className="dropdown-item" onClick={logout}> <i
									className="fas fa-sign-out-alt"></i>
									<span>Logout</span> </a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
}

export default Header