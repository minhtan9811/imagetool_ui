const initState = {}  

const admin = (state = initState, action) => {
    switch(action.type) {
        case "INIT":
            return {
                avatar: '/assets/img/theme/light/avatar.png',
                name: 'Nhan Nguyen',
            };
        case "UDPATE_PROFILE":
            return {
                ...state,
                ...action.payload
            };
        default: 
            return state;
    }
}

export default admin