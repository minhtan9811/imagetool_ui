import { combineReducers } from 'redux';
import user from './User';
import request from './Request';
import admin from './Admin';

const reducers = combineReducers({
    User: user,
    Admin: admin,
    Request: request
});

export default reducers;