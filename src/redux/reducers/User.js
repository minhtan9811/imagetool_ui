const initState = {}

const user = (state = initState, action) => {
	switch (action.type) {
		case "INIT":
			return {
				avatar: '/public/img/theme/light/avatar.png',
				name: 'Nhan Nguyen',
				// ta muốn set State ở đây 

			};
		case "INCREASE_WALLET":
			return {
				...state,
				wallet: state.wallet + action.payload
			};
		case "UDPATE_PROFILE":
			return {
				avatar: '/assets/img/theme/light/avatar.png',
				...state,
				...action.payload
			};
		default:
			return state;
	}
}

export default user