const initState = [
    {
        id: 107574880320060,
        createdAt: new Date(),
        url: "https://www.facebook.com/nyan.nguyenn/posts/107574880320060",
        value: 5,
        type: "post",
        totalLike: 100,
        budget: 1000
    },
    {
        id: 2834507740117318,
        createdAt: new Date(),
        url: "https://www.facebook.com/UEVParis/posts/2834507740117318",
        value: 5,
        type: "post",
        totalLike: 20,
        budget: 1500
    },
    {
        id: 1215231695346899,
        createdAt: new Date(),
        url: "https://www.facebook.com/nyan.ntn/posts/1215231695346899",
        value: 5,
        type: "post",
        totalLike: 60,
        budget: 800
    },
    {
        id: 1651118728394603,
        createdAt: new Date(),
        url: "https://www.facebook.com/gearvnhcm/posts/1651118728394603",
        value: 5,
        type: "post",
        totalLike: 60,
        budget: 800
    },
    {
        id: 2495484890750302,
        createdAt: new Date(),
        url: "https://www.facebook.com/groups/vietnamSFF/permalink/2495484890750302/",
        value: 5,
        type: "post",
        totalLike: 60,
        budget: 800
    },
]

const request = (state = initState, action) => {
    switch(action.type) {
        case "UDPATE_REQUEST_LIST":
            return [
                ...action.payload
            ];
        case "REMOVE_REQUEST" :
            return [
                ...state.slice(0,action.payload),
                ...state.slice(action.payload+1)
            ];
        case "ADD_REQUEST": 
            return [
                ...state,
                action.payload
            ];
        default: 
            return state;
    }
}

export default request