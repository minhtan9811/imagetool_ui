import ClientLayout from "./Layouts/ClientLayout";
import AddFrame from "./views/AddFrame";
import AuthLayout from "./Layouts/AuthLayout";
import Login from "./views/Login";
import NLogin from "./views/NLogin";
import { Navigate } from 'react-router-dom';
import React, { Component } from 'react';
import Home from "./views/Home";

const routes = [
	{
		path: '/app',
		element: <ClientLayout />,
		children: [
			{ path: '/overview', element: <Home /> },
			{ path: '/add-frame-page', element: <AddFrame /> },
			{ path: '*', element: <></> }
		]
	},
	{
		path: '/',
		element: <AuthLayout />,
		children: [
			{ path: '/login', element: <NLogin /> },
			{ path: '/', element: <Navigate to="/login" /> },
		]
	},
];
export default routes;