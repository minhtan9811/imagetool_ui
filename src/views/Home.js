import React from 'react';
import {Link} from "react-router-dom";

function Home(){
    return(
        <div className="page-content">
            <div className="min-vh-100 py-5 d-flex align-items-center">
                <div className="w-100">
                    <div className="row justify-content-center">
                        <div className="col-sm-8 col-lg-5 col-xl-4">
                            <div className="card shadow zindex-100 mb-0">
                                <div className="card-body px-md-5 py-5">
                                    <div className="mb-5">
                                        <h6 className="h3">Add Frame to Image</h6>
                                        <p className="text-muted mb-0">Together we make beautiful photos</p>
                                    </div>
                                    <span className="clearfix"></span>
                                    <div className="mt-4">
                                        <Link className="btn btn-sm btn-primary btn-icon rounded-pill center" to="/app/add-frame-page">
                                            <span className="btn-inner--text">Go to Add Frame</span>
                                            <span className="btn-inner--icon">
                                                <i className="fas fa-long-arrow-alt-right"></i>
                                            </span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Home