import React, { useRef } from 'react'
import Konva from "konva";
import frameAvatar from './../frame_ava.png';
import frameHorizontal from './../Frame_horizontal.png';
import frameVertical from './../Frame_vertical.png';
import API from '../utils/api'

function AddFrame() {
	const formRef = useRef()
	var width = window.innerWidth;
	var height = window.innerHeight;
	var _URL = window.URL || window.webkitURL;
	var imageObj1 = new Image();

	var imageObj2 = new Image();

	var layer = new Konva.Layer();

	var darthVaderGroup = new Konva.Group({
		x: 180,
		y: 100,
		draggable: true,
	});

	var yodaGroup = new Konva.Group({
		x: 100,
		y: 110,
		draggable: true,
	});


	var combineGroup = new Konva.Group({
		x: 100,
		y: 100,
	});

	var tr_id = null;
	var tr1_id = null;


	return (
		<div className="page-content">
			<div className="page-title">
				<div className="row justify-content-between align-items-center">
					<div className="col-md-6 mb-3 mb-md-0">
					</div>
				</div>
			</div>
			<div className="col-xl-12 col-lg-12 col-md-12">
				<div className="card ">
					<div className="card-header">
						<h4 className="mb-0 text-center">Add Frame To Image</h4>
					</div>
					<div className="card-body d-flex" style={{ padding: 0 }}>
						<div className="col-lg-3 col-md-3 scroll-list" id="select-frame">
							<h5 className="mb-2 mt-3">Select Frame</h5>
							<div className="h6 d-none" id="warning" style={{ color: "red" }}>*Please upload your image</div>
							<div className="list-group square-image">
								<a className="list-group-item list-group-item-action d-flex align-items-center active"
									data-image={frameVertical} onClick={selectFrame}>
									<img alt="Image placeholder"
										src={frameVertical}
										className="avatar  rounded-circle mr-3"
										data-image={frameVertical} />
									<span className="text-truncate" data-image={frameVertical}>Vertical Frame</span>
								</a>
							</div>
							<div className="list-group square-image">
								<a className="list-group-item list-group-item-action d-flex align-items-center"
									data-image={frameHorizontal} onClick={selectFrame}>
									<img alt="Image placeholder"
										src={frameHorizontal}
										className="avatar  rounded-circle mr-3"
										data-image={frameHorizontal} />
									<span className="text-truncate" data-image={frameHorizontal}>Horizontal Frame</span>
								</a>
							</div>
							<div className="list-group square-image">
								<a className="list-group-item list-group-item-action d-flex align-items-center"
									data-image={frameAvatar} onClick={selectFrame}>
									<img alt="Image placeholder"
										src={frameAvatar}
										className="avatar  rounded-circle mr-3"
										data-image={frameAvatar} />
									<span className="text-truncate" data-image={frameAvatar}>Avatar Frame</span>
								</a>
							</div>
						</div>
						<div className="col-lg-9 col-md-9 scroll-list">
							<h5 className="mb-2 mt-3">Upload your Image</h5>
							<div className="h6" style={{ color: "red" }}>*Please upload your image before selecting a frame</div>
							<div className="col-xl-6 center" style={{ top: "30%" }}>
								<div className="mt-4" id="upload-group">
									We will display a doodle here
								</div>
							</div>
							<div id="container" className="scroll-list-container"></div>
						</div>
					</div>
					<div className="card-footer px-md-5 d-flex justify-content-end">
						<div className="mr-2 col-6 col-xs-6 col-md-2 col-lg-2">
							<input type="file" name="file-2[]"
								id="file-2"
								className="custom-input-file custom-input-file--2"
								accept="image/x-png,image/gif,image/jpeg"
								onChange={(e) => displayPreview(e)} />
							<label htmlFor="file-2"
								className="btn btn-sm btn-primary btn-icon rounded-pill">
								<span>Choose a file…</span>
								<i className="fa fa-upload"></i>
							</label>
						</div>

						<button type="button"
							className="btn btn-sm btn-primary btn-icon rounded-pill float-right text-truncate" onClick={exportImage}>
							<span className="btn-inner--text">Export Image</span>
							<span className="btn-inner--icon"><i className="fas fa-download"></i></span>
						</button>
					</div>
				</div>
			</div>
			<form id="img-Form"></form>
		</div>
	);


	function displayPreview(e) {
		var element = document.getElementById("warning");
		element.classList.add("d-none")
		const file = e.target.files[0]

		var stage = new Konva.Stage({
			container: "container",
			width: 900,
			height: 900,
		});

		if (layer.parent === null) {

		}


		stage.add(layer);
		layer.add(combineGroup)

		if (darthVaderGroup.children.length === 1) {
			var result = null;
			darthVaderGroup.children.splice(0, 1)
			layer.children.forEach((child, index) => {
				if (child._id === tr1_id) {
					result = index
				}
			})
			layer.children.splice(result, 1)
		}


		imageObj1.onload = function () {
			var ratio = imageObj1.width / imageObj1.height;
			var darthVaderImg = new Konva.Image({
				width: 300 * ratio,
				height: 300,
			});
			layer.add(darthVaderGroup);
			darthVaderGroup.add(darthVaderImg);
			darthVaderImg.image(imageObj1);
			const tr1 = new Konva.Transformer({
				node: darthVaderGroup,
				keepRatio: true,
				enabledAnchors: ['top-left', 'top-right', 'bottom-left', 'bottom-right'],
			});
			layer.add(tr1);
			layer.draw();
			tr1_id = tr1._id;
		};
		imageObj1.src = _URL.createObjectURL(file);
		var element = document.getElementById("upload-group");
		element.classList.add("d-none")
	}

	function selectFrame(e) {
		if (layer.children.length === 0) {
			var element = document.getElementById("warning");
			element.classList.remove("d-none")
		} else {
			// console.log(e.target)
			var url = e.target.getAttribute("data-image")
			// console.log(url)

			//remove transformer when reslect frame
			if (yodaGroup.children.length === 1) {
				var result = null;
				yodaGroup.children.splice(0, 1)
				layer.children.forEach((child, index) => {
					if (child._id === tr_id) {
						result = index
					}
				})
				layer.children.splice(result, 1)
			}

			imageObj2.onload = function () {
				var ratio = imageObj2.width / imageObj2.height;
				var yodaImg = new Konva.Image({
					width: 300 * ratio,
					height: 300,
				});
				layer.add(yodaGroup);
				yodaGroup.add(yodaImg);
				yodaImg.image(imageObj2);
				const tr = new Konva.Transformer({
					node: yodaGroup,
					keepRatio: true,
					enabledAnchors: ['top-left', 'top-right', 'bottom-left', 'bottom-right'],
				});
				layer.add(tr);
				layer.draw();
				tr_id = tr._id
			};
			imageObj2.setAttribute('crossOrigin', 'anonymous');
			imageObj2.src = url;
		}
	}


	//function convert base64 to file
	function dataURLtoFile(dataurl, filename) {

		var arr = dataurl.split(','),
			mime = arr[0].match(/:(.*?);/)[1],
			bstr = atob(arr[1]),
			n = bstr.length,
			u8arr = new Uint8Array(n);

		while (n--) {
			u8arr[n] = bstr.charCodeAt(n);
		}

		return new File([u8arr], filename, { type: mime });
	}

	function b64toBlob(b64Data, contentType, sliceSize) {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;

		var byteCharacters = atob(b64Data);
		var byteArrays = [];

		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);

			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}

			var byteArray = new Uint8Array(byteNumbers);

			byteArrays.push(byteArray);
		}

		var blob = new Blob(byteArrays, { type: contentType });
		return blob;
	}


	function exportImage() {

		combineGroup.add(darthVaderGroup, yodaGroup);

		//get dataurl
		var dataURL = combineGroup.toDataURL({
			pixelRatio: window.devicePixelRatio
		});
		// console.log(combineGroup)
		if (dataURL !== "data:,") {
			//dowload file
			var link = document.createElement('a');
			link.download = "image";
			link.href = dataURL;
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);

			//Clear layer
			layer.destroyChildren();
			layer.draw();

			// Get the form
			var form = document.getElementById("img-Form");
			var ImageURL = dataURL;
			// Split the base64 string in data and contentType
			var block = ImageURL.split(";");
			// Get the content type
			var contentType = block[0].split(":")[1];// In this case "image/gif"
			// get the real base64 content of the file
			var realData = block[1].split(",")[1];// In this case "iVBORw0KGg...."

			// Convert to blob
			var blob = b64toBlob(realData, contentType);

			// Create a FormData and append the file
			var fd = new FormData(form);
			fd.append("image", blob);

			// Submit Form and upload file

			var token = sessionStorage.getItem('like2like_token');
			API.instance.defaults.headers.authorization = token;
			API.upload(fd)
				.then((response) => {
					console.log(response.data);
					//dispatch(initAction());
				})
				.catch((err) => {
					console.log('API err', err)
				})


		}

	}
}

export default AddFrame