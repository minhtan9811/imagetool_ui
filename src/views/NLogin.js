import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { initAction, updateProfile } from '../redux/actions/User';
import API from '../utils/api'

const Login = () => {
	const navigate = useNavigate();
	const user = useSelector(state => state.User);
	const dispatch = useDispatch();
	const { t } = useTranslation()

	useEffect(() => {
		if (user.username) {
			navigate('/app/overview', { replace: true });
		} else {
			window.fbAsyncInit = function () {
				window.FB.init({
					appId: '819999065506900',
					cookie: true,  // enable cookies to allow the server to access
					xfbml: true,  // parse social plugins on this page
					version: 'v9.0' // use version 2.1
				});
			};

			// Load the SDK asynchronously
			(function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/en_US/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		}
		return () => {
			console.log("cleaned up");
		};
	}, []);

	const statusChangeCallback = (response) => {
		console.log('statusChangeCallback', response.status)
		if (response.status === 'connected') {
			console.log("connected");
			console.log(response);

			window.FB.api('/me', function (response) {
				sessionStorage.setItem('name', response.name);
			});
			loginApp(response);
		} else if (response.status === 'not_authorized') {
			console.log('Please log into this app.');
		} else {
			console.log('Please log into Facebook.');
		}
	};

	const loginApp = (res) => {
		API.login({ accessToken: res.authResponse.accessToken, type: 'facebook_login' })
			.then((response) => {

				console.log("login to server successfully" + response);
				sessionStorage.setItem('like2like_token', response.data.token);
				console.log(response.data.token);
				//set defaults header
				API.instance.defaults.headers.authorization = response.data.token;
				dispatch(initAction());
				if (response.data.token) {
					navigate('/app/add-frame-page', { replace: true });
				} else {
					API.me().then((responseMe) => {
						dispatch(updateProfile(responseMe.data))
						navigate('/app/add-frame-page', { replace: true });
					})
				}
			})
			.catch((err) => {
				console.log('API err', err)
			})
	}

	const onLogin = () => {
		window.FB.login(function (response) {
			if (response.authResponse) {
				statusChangeCallback(response);
			} else {
				console.log('User cancelled login or did not fully authorize.');
			}
		});
	}

	return (
		<>
			<div className="min-vh-100 py-5 d-flex align-items-center">
				<div className="w-100">
					<div className="row justify-content-center">
						<div className="col-sm-8 col-lg-4">
							<div className="card shadow zindex-100 mb-0">
								<div className="card-body px-md-5 py-5">
									<div className="mb-5">
										<h6 className="h3">{t('Login')}</h6>
										<p className="text-muted mb-0">{t('Sign in to your account to continue')}.</p>
									</div>
									<span className="clearfix"></span>
									<div className="mt-4">
										<button type="button" onClick={onLogin} className="btn btn-sm btn-primary btn-icon rounded-pill w-100">
											<span className="btn-inner--icon"><i className="fab fa-facebook-f"></i></span>
											<span className="btn-inner--text">{t('Sign in with Facebook')}</span>
											<span className="btn-inner--icon"><i className="fas fa-long-arrow-alt-right"></i></span>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}

export default Login