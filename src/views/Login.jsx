import React from 'react';

function Login(){
    return(
        <div className="page-content">
            <div className="min-vh-100 py-5 d-flex align-items-center">
                <div className="w-100">
                    <div className="row justify-content-center">
                        <div className="col-sm-8 col-lg-5 col-xl-4">
                            <div className="card shadow zindex-100 mb-0">
                                <div className="card-body px-md-5 py-5">
                                    <div className="mb-5">
                                        <h6 className="h3">Add Frame to Image</h6>
                                        <p className="text-muted mb-0">Please login with Facebook</p>
                                    </div>
                                    <span className="clearfix"></span>
                                    <div className="mt-4">
                                        <button type="button"
                                                className="btn btn-sm btn-primary btn-icon rounded-pill center">
                                              <span className="btn-inner--icon">
                                                  <i className="fab fa-facebook-f"></i>
                                              </span>
                                            <span className="btn-inner--text">Login with Facebook</span>
                                            <span className="btn-inner--icon">
                                                <i className="fas fa-long-arrow-alt-right"></i>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Login