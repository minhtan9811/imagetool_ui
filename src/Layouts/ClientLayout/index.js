import React from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import Header from "../../components/Header";
import SideBar from "../../components/SideBar";
import { useSelector } from "react-redux";
import { useEffect, useState } from 'react';


const ClientLayout = () => {
	const [name] = useState(sessionStorage.getItem('name'));

	const user = useSelector(state => state.User);
	const navigate = useNavigate();
	useEffect(() => {
		if (!name) {
			navigate('/login', { replace: true });
		}
	}, [name]);

	return (
		<div className="container-fluid container-application">
			<SideBar />
			<div className="main-content position-relative">
				<Header />
				<Outlet />
			</div>
		</div>
	);
};

export default ClientLayout;
